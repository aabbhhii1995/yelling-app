import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class YellingTest {

	Yelling yell = new Yelling();
	String name = "output";

	@Before
	public void setUp() throws Exception {
	}

	@Test
	// Requirement 1 : one person is yelling

	public void testOnePersonYelling() {
		// Enter the name of your choice below
		String[] name1 = { "Peter" };
		String name = yell.Scream(name1);
		assertEquals(name1[0] + " is yelling", name);
	}

	@Test
	// Requirement 2 : when name = null
	// expected output = "Nobody is yelling"

	public void testNobodyYelling() {
		String[] name1 = { "null" };
		String name = yell.Scream(name1);
		assertEquals("Nobody is yelling", name);
	}

	@Test
	// Requirement 3 : when name is in uppercase
	// input = "PETER"
	// expected output = "PETER IS YELLING"

	public void testUpperCase() {
		String[] name1 = { "PETER" };
		String name = yell.Scream(name1);
		assertEquals(name1[0] + " IS YELLING", name);
	}

	@Test
	// Requirement 4 : when two people are yelling
	public void testTwoPersonsYelling() {
		String[] name1 = { "Peter", "Kadeem " };

		// To check input 2 as per the requirement --> uncomment the below line and
		// comment the upper line
		// String[] name1 = {"Albert"," Pritesh "};

		String name = yell.Scream(name1);
		assertEquals(name1[0] + " and" + name1[1] + "are yelling", name);
	}

	@Test
	// Requirement 5 : if more than 2 people are yelling

	public void testMoreThanTwoPersonsYelling() {

		String[] name1 = { "Peter", " Kadeem", " Albert " };
		String name = yell.Scream(name1);
		assertEquals(name1[0] + "," + name1[1] + ", and" + name1[2] + "are yelling", name);
	}

	@Test // Requirement 6 : if more than 2 people are yelling //
	// array is mixed with upper case strings and lower case

	public void testMixedYelling() {
		String[] name1 = { "Peter", "EMAD", "Albert", "Kadeem" };
		String name = yell.Scream(name1);
		assertEquals("Peter, Albert and Kadeem are yelling. SO IS EMAD!", name);
	}

}
